module gitlab.com/simon.macklin/polls

go 1.14

require (
	github.com/dimiro1/health v0.0.0-20191019130555-c5cbb4d46ffc
	github.com/golang-migrate/migrate/v4 v4.12.2
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/vault/api v1.0.4
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.8.0
	github.com/prometheus/client_golang v1.7.1
	github.com/stretchr/testify v1.6.1
	go.uber.org/zap v1.15.0
)
