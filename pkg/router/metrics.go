package router

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	httpBadRequest = promauto.NewCounter(prometheus.CounterOpts{
		Name:      "http_bad_requests_total",
		Help:      "total bad requests",
		Namespace: "polls",
	})
	httpStatusOK = promauto.NewCounter(prometheus.CounterOpts{
		Name:      "http_status_ok_total",
		Help:      "total successfull requests",
		Namespace: "polls",
	})
	httpInternalServerError = promauto.NewCounter(prometheus.CounterOpts{
		Name:      "http_internal_server_error_total",
		Help:      "total internal server errors",
		Namespace: "polls",
	})
)
