package router

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/simon.macklin/polls/pkg/poll"
	"go.uber.org/zap"
)

type ValidationErrorsResponse struct {
	Errors []error `json:"errors"`
}

type NewPollResponse struct {
	ID        int
	StartTime time.Time
	EndTime   time.Time
}

type GetPolls struct {
	ID        string
	StartTime time.Time
	EndTime   time.Time
}

func getPoll(s poll.Service, log *zap.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := r.Context().Value("request_id").(string)
		log := log.With(zap.String("request_id", id))

		v := mux.Vars(r)
		id, ok := v["id"]
		if !ok {
			http.Error(w, "missing poll id", http.StatusBadRequest)
			log.Info("poll can not be fetched without an id")
			return
		}

		idn, err := strconv.Atoi(id)
		if err != nil {
			httpBadRequest.Inc()
			http.Error(w, "invalid id, ids should be of type int", http.StatusBadRequest)
			log.Error("failed to parse poll id", zap.Error(err))
			return
		}

		if idn == 0 {
			httpBadRequest.Inc()
			http.Error(w, "invalid id, ids are only positive numbers and start at 1", http.StatusBadRequest)
			log.Info("invalid id, ids are only positive numbers and start at 1")
			return
		}

		p, err := s.GetByID(idn)
		if err != nil {
			switch err {
			case poll.ErrPollNotFound:
				http.Error(w, "poll not found", http.StatusNotFound)
				log.Info("poll not found", zap.Int("poll_id", idn))
				return
			default:
				http.Error(w, "something went wrong", http.StatusInternalServerError)
				log.Error("failed to get poll", zap.Int("poll_id", idn), zap.Error(err))
				return
			}
		}

		b, err := json.Marshal(p)
		if err != nil {
			http.Error(w, "failed to encode response", http.StatusInternalServerError)
			log.Error("failed to encode response", zap.Error(err))
			httpInternalServerError.Inc()
			return
		}
		w.Write(b)
		httpStatusOK.Inc()
		return
	}
}

func getPolls(s poll.Service, log *zap.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := r.Context().Value("request_id").(string)
		log := log.With(zap.String("request_id", id))

		all, err := s.GetAll()
		if err != nil {
			switch err {
			case poll.ErrNoPollsFound:
				log.Info("no polls found", zap.Error(err))
				w.Write([]byte("no polls created yet"))
				httpStatusOK.Inc()
				return
			default:
				http.Error(w, "failed to get polls", http.StatusInternalServerError)
				log.Error("failed to retreive polls", zap.Error(err))
				httpInternalServerError.Inc()
				return
			}
		}

		resp, err := json.Marshal(all)
		if err != nil {
			http.Error(w, "something went wrong", http.StatusInternalServerError)
			log.Error("failed to marhsall response", zap.Error(err))
			httpInternalServerError.Inc()
			return
		}

		w.Write(resp)
		httpStatusOK.Inc()
		return
	}
}

func addPoll(s poll.Service, log *zap.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		id := r.Context().Value("request_id").(string)
		log := log.With(zap.String("request_id", id))

		body := json.NewDecoder(r.Body)
		var p poll.Poll

		err := body.Decode(&p)
		if err != nil {
			http.Error(w, "failed to decode response body", http.StatusBadRequest)
			log.Error("failed to decode response body", zap.Error(err))
			httpBadRequest.Inc()
			return
		}

		errs := p.IsValid()
		if len(errs) > 0 {
			log.Info("request failed validation", zap.Errors("errors", errs))
			w.WriteHeader(http.StatusBadRequest)
			err = json.NewEncoder(w).Encode(ValidationErrorsResponse{errs})
			if err != nil {
				log.Error("failed to send response", zap.Error(err))
			}
			httpBadRequest.Inc()
			return
		}

		n, err := s.Add(p)
		if err != nil {
			http.Error(w, "failed to create new poll", http.StatusInternalServerError)
			log.Error("failed to write to the database", zap.Error(err))
			httpInternalServerError.Inc()
			return
		}

		resp := NewPollResponse{
			ID:        n,
			StartTime: p.StartTime.UTC(),
			EndTime:   p.StartTime.Add(p.Duration).UTC(),
		}

		err = json.NewEncoder(w).Encode(resp)
		if err != nil {
			http.Error(w, "failed to send response", http.StatusInternalServerError)
			log.Error("failed to send response", zap.Error(err))
			httpInternalServerError.Inc()
			return
		}

		log.Info("poll created", zap.Int("poll_id", n), zap.String("question", p.Question))
		httpStatusOK.Inc()
		return
	}
}
