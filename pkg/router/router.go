package router

import (
	"github.com/dimiro1/health"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/simon.macklin/polls/pkg/poll"
	"gitlab.com/simon.macklin/polls/pkg/server"
	"gitlab.com/simon.macklin/polls/pkg/vote"
	"go.uber.org/zap"
)

func NewHandler(p poll.Service, v vote.Service, l *zap.Logger) *mux.Router {
	r := mux.NewRouter()
	r.StrictSlash(true)

	v1 := r.NewRoute().Subrouter()

	v1.Use(server.Logger(l))
	v1.Handle("/api/v1/polls", getPolls(p, l)).Methods("GET")
	v1.Handle("/api/v1/poll/{id}", getPoll(p, l)).Methods("GET")
	v1.Handle("/api/v1/poll", addPoll(p, l)).Methods("POST")

	v1.Handle("/api/v1/vote", addVote(v, l)).Methods("POST")
	v1.Handle("/api/v1/votes/{poll_id}/", getVotes(v, l)).Methods("GET")

	info := r.NewRoute().Subrouter()

	live := health.NewHandler()
	live.AddInfo("version", "0.0.1")
	live.AddInfo("app", "polls")
	info.Handle("/live", live)

	info.Handle("/metrics", promhttp.Handler())

	ready := health.NewHandler()
	ready.AddChecker("database", p)
	info.Handle("/ready", ready)

	return r
}
