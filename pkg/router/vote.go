package router

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/simon.macklin/polls/pkg/storage"
	"gitlab.com/simon.macklin/polls/pkg/vote"
	"go.uber.org/zap"
)

type NewVoteResponse struct {
}

func addVote(s vote.Service, log *zap.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		id := r.Context().Value("request_id").(string)
		log := log.With(zap.String("request_id", id))

		d := json.NewDecoder(r.Body)
		var v vote.Vote

		err := d.Decode(&v)
		if err != nil {
			log.Error("failed to decode response", zap.Error(err))
			http.Error(w, "failed to decode response body", http.StatusBadRequest)
			return
		}

		log = log.With(zap.Int("poll_id", v.PollID))

		_, err = s.Add(v)
		if err != nil {
			switch err {
			case storage.ErrInvalidChoice:
				log.Info("the vote was invalid", zap.Error(err))
				http.Error(w, "invalid choice for this poll", http.StatusBadRequest)
				return

			case storage.ErrPollDoesNotExist:
				log.Info("invalid poll id", zap.Error(err))
				http.Error(w, "invalid poll id", http.StatusBadRequest)
				return

			default:
				log.Error("failed to write to the database", zap.Error(err))
				http.Error(w, "failed to create new poll", http.StatusInternalServerError)
				return
			}
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("vote accepted"))

		httpStatusOK.Inc()

		log.Info("vote accepted")
		return
	}
}

func getVotes(s vote.Service, log *zap.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		id := r.Context().Value("request_id").(string)
		log := log.With(zap.String("request_id", id))

		v := mux.Vars(r)
		id, ok := v["poll_id"]
		if !ok {
			httpBadRequest.Inc()
			log.Info("incorrect id requested")
			http.Error(w, "missing poll id", http.StatusBadRequest)
			return
		}

		idn, err := strconv.Atoi(id)
		if err != nil {
			httpBadRequest.Inc()
			http.Error(w, "id should be of type int", http.StatusBadRequest)
			return
		}

		log = log.With(zap.Int("poll_id", idn))
		log.Info("getting votes")

		votes, err := s.GetVotes(idn)
		if err != nil {
			log.Error("failed to retrieve polls from the database", zap.Error(err))
			http.Error(w, "something went wrong", http.StatusBadRequest)
			return
		}

		b, err := json.Marshal(votes)
		if err != nil {
			log.Error("failed to encode response", zap.Error(err))
			http.Error(w, "failed to send response", http.StatusInternalServerError)
			return
		}
		w.Write(b)
		httpStatusOK.Inc()
		log.Info("votes returned successfully")
		return
	}
}
