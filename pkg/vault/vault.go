package vault

import (
	"bytes"
	"fmt"
	vaultapi "github.com/hashicorp/vault/api"
	"io/ioutil"
	"strings"
)

type Config struct {
	Address    string `envconfig:"POLLS_VAULT_ADDR" required:"true"`
	Role       string `envconfig:"POLLS_VAULT_ROLE" required:"true"`
	Token      string `envconfig:"POLLS_VAULT_TOKEN"`
	AuthMethod string `envconfig:"POLLS_VAULT_AUTH_METHOD" default:"kubernetes"`
	AuthPath   string `envconfig:"POLLS_VAULT_AUTH_PATH"`
	JWTPath    string `envconfig:"POLLS_JWT_PATH" default:"/var/run/secrets/kubernetes.io/serviceaccount/token"`
}

type Client struct {
	api   *vaultapi.Client
	cfg   Config
	token string
}

func NewClient(conf Config) (*Client, error) {
	c := &Client{}
	cfg := vaultapi.DefaultConfig()
	cfg.Address = conf.Address
	api, err := vaultapi.NewClient(cfg)
	if err != nil {
		return nil, err
	}
	c.api = api
	c.cfg = conf
	return c, nil
}

func NewAuthenicatedClient(cfg Config) (*Client, error) {
	c, err := NewClient(cfg)
	if err != nil {
		return nil, err
	}
	err = c.Authenicate()
	if err != nil {
		return nil, err
	}
	return c, err
}

func (c *Client) Authenicate() error {
	switch strings.ToLower(c.cfg.AuthMethod) {
	case "kubernetes":
		token, err := c.KubernetesLogin()
		if err != nil {
			return err
		}
		c.api.SetToken(token)
	case "token":
		c.api.SetToken(c.cfg.Token)
	default:
		return fmt.Errorf("auth type %s not suported", c.cfg.AuthMethod)
	}
	return nil
}

func (c *Client) KubernetesLogin() (string, error) {
	f, err := ioutil.ReadFile(c.cfg.JWTPath)
	if err != nil {
		return "", err
	}

	jwt := string(bytes.TrimSpace(f))

	if c.cfg.AuthPath == "" {
		c.cfg.AuthPath = "auth/kubernetes/login"
	}

	secret, err := c.api.Logical().Write(c.cfg.AuthPath, map[string]interface{}{
		"jwt":  jwt,
		"role": c.cfg.Role,
	})
	if err != nil {
		return "", err
	}
	return secret.Auth.ClientToken, nil
}

func (c *Client) Read(path string) (*Secret, error) {
	sec, err := c.api.Logical().Read(path)
	if err != nil {
		return nil, err
	}
	if sec == nil {
		return nil, fmt.Errorf("secret at path %s does not exist", path)
	}

	s := &Secret{
		Data: make(map[string]string),
	}
	s.Renewable = sec.Renewable
	for k, v := range sec.Data {
		s.Data[k] = v.(string)
	}
	return s, nil
}

func (c *Client) NewRenewer() (*vaultapi.Renewer, error) {
	secret, err := c.api.Auth().Token().RenewSelf(0)
	if err != nil {
		return nil, fmt.Errorf("failed to renew token %s", err)
	}
	renewer, err := c.api.NewRenewer(&vaultapi.RenewerInput{Secret: secret})
	if err != nil {
		return nil, fmt.Errorf("failed to load token renewer %s", err)
	}
	return renewer, nil
}
