package vault

import "sync"

type Secret struct {
	Data      map[string]string
	Renewable bool
	m         sync.Mutex
}
