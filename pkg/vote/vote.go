package vote

import (
	"errors"
)

type Vote struct {
	ID     int    `json:"id"`
	PollID int    `json:"poll_id"`
	Choice string `json:"choice"`
	User   string `json:"user"`
}

var (
	ErrNoVotesFound       = errors.New("no votes found")
	ErrPollIDDoesNotExist = errors.New("poll does not exist")
)

func (s *service) Add(v Vote) (int, error) {
	id, err := s.r.AddVote(v)
	if err != nil {
		return id, err
	}
	newVote.Inc()
	return id, nil
}

func (s *service) GetVotes(poll_id int) ([]Vote, error) {
	v, err := s.r.GetVotes(poll_id)
	if err != nil {
		return nil, err
	}
	if len(v) < 1 {
		return nil, ErrNoVotesFound
	}
	return v, nil
}

func GenerateRandomVote() *Vote {
	var v Vote
	return &v
}
