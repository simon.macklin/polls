package vote

type Service interface {
	Add(v Vote) (int, error)
	GetVotes(poll_id int) ([]Vote, error)
}

type Repository interface {
	AddVote(v Vote) (int, error)
	GetVotes(poll_id int) ([]Vote, error)
}

type service struct {
	r Repository
}

func NewService(re Repository) Service {
	return &service{r: re}
}
