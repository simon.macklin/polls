package vote

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	newVote = promauto.NewCounter(prometheus.CounterOpts{
		Name:      "votes_total",
		Help:      "total votes the system as accepted",
		Namespace: "polls",
	})
)
