package config

import (
	"errors"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/simon.macklin/polls/pkg/storage/postgres"
	"gitlab.com/simon.macklin/polls/pkg/vault"
)

type Config struct {
	Postgres postgres.Config
	Vault    vault.Config
}

func New() (Config, error) {
	var cfg Config
	err := envconfig.Process("polls", &cfg)
	if err != nil {
		return cfg, err
	}
	return cfg, nil
}

// TODO
func (c *Config) Valid() (bool, []error) {
	errs := []error{}
	if c.Vault.AuthMethod == "kubernetes" {
		if c.Vault.Role == "" {
			errs = append(errs, errors.New("when kubernetes auth method used role is required"))
		}
	}
	return true, errs
}
