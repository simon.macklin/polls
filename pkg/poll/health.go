package poll

import "github.com/dimiro1/health"

func (s *service) Check() health.Health {
	h := health.NewHealth()
	stats, err := s.r.Healthy()
	if err != nil {
		h.Down()
		h.AddInfo("error", err)
		return h
	}
	for k, v := range stats {
		h.AddInfo(k, v)
	}
	h.Up()
	return h
}
