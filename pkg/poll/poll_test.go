package poll

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type mockRepository struct{}

func (m *mockRepository) AddPoll(p Poll) (int, error) {
	return 0, nil
}

func (m *mockRepository) GetPollByID(id int) (Poll, error) {
	var p Poll
	return p, nil
}

func (m *mockRepository) GetAllPolls() ([]Poll, error) {
	var p []Poll
	return p, nil
}

func (m *mockRepository) Healthy() (map[string]interface{}, error) { return nil, nil }

func TestIsValid(t *testing.T) {
	var tt = []struct {
		name string
		poll Poll
		errs []error
	}{
		{name: "should pass", poll: Poll{ID: 1, Question: "will this pass?", Answers: []string{"yes", "no"}}, errs: []error{}},
		{name: "no question", poll: Poll{ID: 2, Answers: []string{"yes", "no"}}, errs: []error{ErrMissingQuestion}},
		{name: "no answers", poll: Poll{ID: 3, Question: "will this fail?"}, errs: []error{ErrMissingAnswers}},
	}

	for _, tst := range tt {
		t.Run(tst.name, func(t *testing.T) {
			assert.EqualValues(t, tst.errs, tst.poll.IsValid())
		})
	}
}

func TestAddPoll(t *testing.T) {
	var tt = []struct {
		name   string
		poll   Poll
		errStr string
	}{
		{name: "good", poll: Poll{Question: "will this test pass?", Answers: []string{"yes", "no"}}},
		{name: "missing question", poll: Poll{Answers: []string{"yes", "no"}}, errStr: "requires a question"},
		{name: "missing answers", poll: Poll{Question: "will this test pass?"}, errStr: "missing answers"},
	}

	s := service{r: &mockRepository{}}

	for _, tst := range tt {
		t.Run(tst.name, func(t *testing.T) {
			id, err := s.Add(tst.poll)
			if err != nil {
				assert.EqualError(t, err, tst.errStr)
			}
			assert.GreaterOrEqual(t, 1, id)
		})
	}
}

func TestGetAllPolls(t *testing.T) {
	var tt = []struct {
		name string
		poll Poll
		errs []error
	}{}

	for _, tst := range tt {
		t.Run(tst.name, func(t *testing.T) {

		})
	}
}

func TestGetByID(t *testing.T) {
	var tt = []struct {
		name string
		poll Poll
		errs []error
	}{}

	for _, tst := range tt {
		t.Run(tst.name, func(t *testing.T) {

		})
	}
}
