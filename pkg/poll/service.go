package poll

import (
	"github.com/dimiro1/health"
	"go.uber.org/zap"
)

type Service interface {
	Add(p Poll) (int, error)
	GetByID(id int) (Poll, error)
	GetAll() ([]Poll, error)
	Check() health.Health
}

type Repository interface {
	AddPoll(p Poll) (int, error)
	GetPollByID(id int) (Poll, error)
	GetAllPolls() ([]Poll, error)
	Healthy() (map[string]interface{}, error)
}

type service struct {
	r      Repository
	logger *zap.Logger
}

func NewService(re Repository, l *zap.Logger) Service {
	return &service{r: re, logger: l}
}
