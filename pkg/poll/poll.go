package poll

import (
	"errors"
	"time"
)

var (
	ErrPollNotFound    = errors.New("poll not found")
	ErrMissingQuestion = errors.New("requires a question")
	ErrMissingAnswers  = errors.New("requires at least two valid answers")
	ErrNoPollsFound    = errors.New("no polls found")
)

type Vote struct {
	Choice string `json:"choice"`
	User   string `json:"user"`
}

type Poll struct {
	ID         int           `json:"id"`
	Question   string        `json:"question"`
	StartTime  time.Time     `json:"start"`
	Duration   time.Duration `json:"ends"`
	Answers    []string      `json:"answers"`
	TotalVotes int           `json:"total_votes"`
	Votes      []Vote        `json:"votes"`
}

func (p *Poll) IsValid() []error {
	errs := []error{}

	if p.Question == "" {
		errs = append(errs, ErrMissingQuestion)
	}
	if len(p.Answers) < 2 {
		errs = append(errs, ErrMissingAnswers)
	}
	return errs
}

func (s *service) Add(p Poll) (int, error) {
	id, err := s.r.AddPoll(p)
	if err != nil {
		return id, err
	}
	newPoll.Inc()
	return id, nil
}

func (s *service) GetAll() ([]Poll, error) {
	p, err := s.r.GetAllPolls()
	if err != nil {
		return nil, err
	}
	if len(p) < 1 {
		return nil, ErrNoPollsFound
	}
	return p, nil
}

func (s *service) GetByID(id int) (Poll, error) {
	p, err := s.r.GetPollByID(id)
	if err != nil {
		return p, nil
	}
	if p.ID == 0 {
		return p, ErrPollNotFound
	}
	return p, err
}
