package poll

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	newPoll = promauto.NewCounter(prometheus.CounterOpts{
		Name:      "poll_total",
		Help:      "total amount of polls",
		Namespace: "polls",
	})
)
