package server

import (
	"context"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

type Server struct {
	http   *http.Server
	router *mux.Router
	logger *zap.Logger
}

func New(r *mux.Router, l *zap.Logger) *Server {
	p, ok := os.LookupEnv("HTTP_ADDR")
	if !ok {
		p = ":8080"
	}
	return &Server{
		http: &http.Server{
			Addr:    p,
			Handler: r,
		},
		router: r,
		logger: l,
	}
}

func (s *Server) ListenAndServe(ctx context.Context) error {
	s.logger.Info("server starting", zap.String("port", "8080"))

	sig := make(chan os.Signal)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	ctx, cancel := context.WithCancel(ctx)

	go func() {
		signal := <-sig
		s.logger.Info("received signal to shutdown", zap.String("signal", signal.String()))
		defer cancel()
	}()

	go func() error {
		<-ctx.Done()
		return s.http.Shutdown(ctx)
	}()
	return s.http.ListenAndServe()
}
