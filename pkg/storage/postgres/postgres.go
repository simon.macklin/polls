package postgres

import (
	"database/sql"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

type Config struct {
	Host      string `envconfig:"POLLS_DB_HOST" required:"true"`
	Database  string `envconfig:"POLLS_DATABASE" required:"true"`
	Port      int    `envconfig:"POLLS_DB_PORT" default:"5432"`
	User      string
	Password  string
	VaultPath string `envconfig:"POLLS_VAULT_DB_PATH"`
}

func (c *Config) VaultNeeded() bool {
	if c.User == "" {
		return true
	}

	if c.Password == "" {
		return true
	}
	return false
}

type Postgres struct {
	conn *sql.DB
}

func NewSession(cfg Config) (*Postgres, error) {
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s database=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.Database))

	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return &Postgres{conn: db}, nil
}

func (pg *Postgres) Close() {
	pg.conn.Close()
}

func (pg *Postgres) Delete(id string) error { return nil }

func (pg *Postgres) Healthy() (map[string]interface{}, error) {
	stats := make(map[string]interface{})
	err := pg.conn.Ping()
	if err != nil {
		return nil, err
	}
	stats["open_connections"] = pg.conn.Stats().OpenConnections
	stats["wait_count"] = pg.conn.Stats().WaitCount
	stats["wait_duration_ms"] = pg.conn.Stats().WaitDuration.Milliseconds()
	return stats, nil
}

func (pg *Postgres) Migrate() error {
	driver, _ := postgres.WithInstance(pg.conn, &postgres.Config{})
	m, err := migrate.NewWithDatabaseInstance("file:.///../../db/migrations", "postgres", driver)
	if err != nil {
		return err
	}

	if err := m.Up(); err != nil {
		switch err {
		case migrate.ErrNoChange:
			return nil
		default:
			return err
		}
	}

	return nil
}
