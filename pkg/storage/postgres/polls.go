package postgres

import (
	"context"

	"log"

	_ "github.com/lib/pq"
	"gitlab.com/simon.macklin/polls/pkg/poll"
)

func (pg *Postgres) AddPoll(p poll.Poll) (int, error) {

	var id int
	tx, err := pg.conn.BeginTx(context.Background(), nil)
	if err != nil {
		return id, err
	}

	err = tx.QueryRow(`INSERT INTO questions(question, start_time, end_time) VALUES ($1, $2, $3) RETURNING poll_id`,
		p.Question, p.StartTime, p.StartTime.Add(p.Duration)).Scan(&id)

	if err != nil {
		tx.Rollback()
		return id, err
	}

	for _, a := range p.Answers {
		_, err := tx.Exec(`INSERT INTO choices(choice, poll_id) VALUES ($1, $2)`, a, id)
		if err != nil {
			return id, tx.Rollback()
		}
	}

	err = tx.Commit()
	if err != nil {
		return id, err
	}
	return id, nil
}

func (pg *Postgres) GetPollByID(id int) (poll.Poll, error) {

	var p poll.Poll
	err := pg.conn.QueryRow("SELECT poll_id, question, start_time FROM questions WHERE poll_id=$1", id).Scan(&p.ID, &p.Question, &p.StartTime)
	if err != nil {
		return p, err
	}

	var n int
	err = pg.conn.QueryRow("SELECT count(*) FROM votes WHERE poll_id=$1", id).Scan(&n)
	p.TotalVotes = n

	rows, err := pg.conn.Query("SELECT choice, username FROM votes WHERE poll_id=$1", id)
	defer rows.Close()
	if err != nil {
		return p, nil
	}

	votes := []poll.Vote{}
	for rows.Next() {
		v := poll.Vote{}
		err = rows.Scan(&v.User, &v.Choice)
		if err != nil {
			log.Fatal(err)
		}
		votes = append(votes, v)
	}
	p.Votes = votes
	return p, err
}

func (pg *Postgres) GetAllPolls() ([]poll.Poll, error) {
	var polls []poll.Poll
	rows, err := pg.conn.Query("SELECT poll_id, question, start_time FROM questions")
	if err != nil {
		return polls, nil
	}
	for rows.Next() {
		p := poll.Poll{}
		err = rows.Scan(&p.ID, &p.Question, &p.StartTime)
		if err != nil {
			return polls, err
		}
		polls = append(polls, p)
	}
	if len(polls) < 1 {
		return polls, poll.ErrNoPollsFound
	}
	defer rows.Close()
	return polls, nil
}
