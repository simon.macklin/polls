package postgres

import (
	"fmt"

	"github.com/lib/pq"
	_ "github.com/lib/pq"

	"gitlab.com/simon.macklin/polls/pkg/storage"
	"gitlab.com/simon.macklin/polls/pkg/vote"
)

func (pg *Postgres) AddVote(v vote.Vote) (int, error) {
	var id int
	err := pg.conn.QueryRow(`INSERT INTO votes(choice, poll_id, username) VALUES ($1, $2, $3) RETURNING id`, v.Choice, v.PollID, v.User).Scan(&id)
	if err != nil {
		if err, ok := err.(*pq.Error); ok {
			switch err.Message {
			case fmt.Sprintf("%s is not a valid choice", v.Choice):
				return id, storage.ErrInvalidChoice
			case fmt.Sprintf("poll id %d does not exist", v.PollID):
				return id, storage.ErrPollDoesNotExist
			}
		}
		return id, err
	}
	return id, nil
}

func (pg *Postgres) GetVotes(poll_id int) ([]vote.Vote, error) {
	votes := []vote.Vote{}
	rows, err := pg.conn.Query("SELECT choice, username FROM votes WHERE poll_id=$1", poll_id)
	defer rows.Close()
	if err != nil {
		fmt.Println(err)
		return votes, err
	}
	for rows.Next() {
		v := vote.Vote{}
		err = rows.Scan(&v.User, &v.Choice)
		if err != nil {
			fmt.Println(err)
			return votes, err
		}
		votes = append(votes, v)
	}
	return votes, nil
}

func (pg *Postgres) GetVotesCount(poll_id int) (int, error) {
	var n int
	err := pg.conn.QueryRow("SELECT count(*) FROM votes WHERE poll_id=$1", poll_id).Scan(&n)
	return n, err
}
