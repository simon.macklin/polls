package storage

import (
	"errors"
)

var (
	ErrNotFound          = errors.New("record not found")
	ErrIDAlreadyExists   = errors.New("the unique id already exists from another record")
	ErrVoteAlreadyExists = errors.New("the question already exists")
	ErrInvalidChoice     = errors.New("the choice you made is not allowed")
	ErrPollDoesNotExist  = errors.New("poll id does not exist")
)
