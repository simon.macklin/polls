
CREATE TABLE IF NOT EXISTS questions(
    poll_id serial PRIMARY KEY,
    question VARCHAR(100),
    start_time time,
    end_time time,
    created_by VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS choices(
    id serial PRIMARY KEY,
    choice VARCHAR(50),
    poll_id serial,
	FOREIGN KEY(poll_id) REFERENCES questions(poll_id)
);

CREATE UNIQUE INDEX posss ON choices(poll_id, choice);

CREATE TABLE IF NOT EXISTS votes(
    id serial PRIMARY KEY,
    choice VARCHAR(50),
    poll_id serial,
    username VARCHAR(50),
    FOREIGN KEY(poll_id) REFERENCES questions(poll_id)
);

CREATE FUNCTION check_choice_is_valid() RETURNS trigger AS $check$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM questions q WHERE q.poll_id = NEW.poll_id) THEN
 			RAISE EXCEPTION 'poll id % does not exist', NEW.poll_id;
		END IF;

        IF NOT EXISTS (SELECT 1 FROM choices c WHERE c.poll_id = NEW.poll_id AND choice = NEW.choice) THEN
 			RAISE EXCEPTION '% is not a valid choice', NEW.choice;
		END IF;
        RETURN NEW;
    END;
$check$ LANGUAGE plpgsql;


CREATE TRIGGER valid_vote_check BEFORE INSERT OR UPDATE ON votes
    FOR EACH ROW EXECUTE PROCEDURE check_choice_is_valid();