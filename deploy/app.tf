
resource "helm_release" "polls" {
  name    = "polls"
  chart   = "redis"
  version = "6.0.1"
  depends_on = [
    module.postgres
  ]
}


