#!/usr/bin/env bash

set -e
go test -race $(go list ./... | grep -v /vendor/) -v -coverprofile .testCoverage.txt
go tool cover -func .testCoverage.txt
rm .testCoverage.txt
