#!/bin/bash

set -e

TAG=""

if [ -z "${CI_COMMIT_TAG}" ]
    then
    TAG="${CI_COMMIT_SHORT_SHA}"
else 
    TAG="${CI_COMMIT_TAG}"
fi

echo $DOCKER_PASSWORD| docker login --username simonmacklin --password-stdin
docker push simonmacklin/polls:${TAG}
