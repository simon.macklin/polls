#!/bin/bash

set -e

TAG=""

if [ -z "${CI_COMMIT_TAG}" ]
    then
    TAG="${CI_COMMIT_SHORT_SHA}"
else 
    TAG="${CI_COMMIT_TAG}"
fi

docker build . -f docker/Dockerfile.polls -t simonmacklin/polls:${TAG}

