#!/bin/bash

minikube status > /dev/null 2>&1 || minikube start --memory 9500 --cpus 4
kubectl label namespace default istio-injection=enabled > /dev/null 2>&1
kubectl get ns istio-system > /dev/null 2>&1 || istioctl install --set profile=default

kubectl apply -f https://raw.githubusercontent.com/istio/istio/master/samples/addons/prometheus.yaml
kubectl apply -f https://raw.githubusercontent.com/istio/istio/master/samples/addons/grafana.yaml
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.7/samples/addons/jaeger.yaml
kubectl apply -f https://raw.githubusercontent.com/weaveworks/flagger/master/artifacts/flagger/crd.yaml
kubectl apply -f https://raw.githubusercontent.com/istio/istio/master/samples/addons/kiali.yaml

helm repo list | grep flagger || helm repo add flagger https://flagger.app && \
	helm upgrade -i flagger flagger/flagger \
		--namespace=istio-system \
		--set crd.create=false \
		--set meshProvider=istio

kubectl apply -f k8/app/postgres -R
kubectl apply -f k8/security/vault -R

echo 'waiting for postgres to become available'
while [ $(kubectl get pods --selector app=postgres -o custom-columns=":status.phase") != "Running" ]; do 
    sleep 5
    echo "still waiting"
done

echo 'waiting for vault to become available'
while [ $(kubectl get pods --selector app=vault -o custom-columns=":status.phase") != "Running" ]; do 
    sleep 5
    echo "still waiting"
done

kubectl exec service/vault -c vault vault auth enable kubernetes
kubectl exec service/vault -c vault vault write auth/kubernetes/config kubernetes_host=https://kubernetes.default.svc.cluster.local kubernetes_ca_cert=@/run/secrets/kubernetes.io/serviceaccount/ca.crt

kubectl exec service/vault -c vault vault secrets enable database
kubectl exec service/vault -c vault vault write database/config/polls \
    plugin_name="postgresql-database-plugin" \
    connection_url="host=postgres  port=5432 user=admin password=admin database=polls sslmode=disable" \
    allowed_roles="test"

kubectl exec service/vault -c vault vault write database/roles/test \
    db_name=polls \
    creation_statements="CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; \
        grant admin TO \"{{name}}\";" \
    default_ttl="20m" \
    max_ttl="1h"

pod=$(kubectl get pods --selector app=vault | cut -d " " -f 1 | tail -n 1)
kubectl -c vault cp hack/policy.hcl $pod:/
kubectl exec service/vault -c vault -it vault policy write test policy.hcl

kubectl exec service/vault -c vault -it vault write auth/kubernetes/role/test bound_service_account_names=polls bound_service_account_namespaces=default policies=test ttl=30m

kubectl apply -f k8 -R
