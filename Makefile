
build:
	@hack/build.sh

push:
	@hack/push.sh

docker_up:
	@docker-compose -f docker/docker-compose.yml up --build 

docker_down:
	@docker-compose -f docker/docker-compose.yml down

minikube:
	@hack/lab.sh

vault_roles:
	@hack/vault.sh

test:
	@hack/coverage.sh

fmt:
	@go fmt ./...

migrate:
	@migrate -path db/migrations -database postgres://admin:admin@127.0.0.1:5432/polls?sslmode=disable up 

e2e:
	echo