package main

import (
	"context"
	"gitlab.com/simon.macklin/polls/pkg/config"
	"gitlab.com/simon.macklin/polls/pkg/poll"
	"gitlab.com/simon.macklin/polls/pkg/router"
	"gitlab.com/simon.macklin/polls/pkg/server"
	"gitlab.com/simon.macklin/polls/pkg/storage/postgres"
	"gitlab.com/simon.macklin/polls/pkg/vault"
	"gitlab.com/simon.macklin/polls/pkg/vote"
	"go.uber.org/zap"
)

func main() {

	ctx := context.Background()
	log, _ := zap.NewProduction()

	cfg, err := config.New()
	if err != nil {
		log.Fatal(err.Error())
	}

	if cfg.Postgres.VaultNeeded() {
		vault, err := vault.NewAuthenicatedClient(cfg.Vault)
		if err != nil {
			log.Fatal(err.Error())
		}

		dbsec, err := vault.Read(cfg.Postgres.VaultPath)
		if err != nil {
			log.Fatal(err.Error())
		}

		cfg.Postgres.User = dbsec.Data["username"]
		cfg.Postgres.Password = dbsec.Data["password"]
	}

	db, err := postgres.NewSession(cfg.Postgres)
	if err != nil {
		log.Fatal(err.Error())
	}

	p := poll.NewService(db, log)
	v := vote.NewService(db)
	s := server.New(router.NewHandler(p, v, log), log)
	if err := s.ListenAndServe(ctx); err != nil {
		log.Fatal(err.Error())
	}
}
